#!/usr/bin/zsh

# nq_package-managers
## oqo_zplug
### Check if zplug is installed
if [[ $Q5q60nq_oq_packagemanager_zsh == "zplug" ]]; then
    # INFO: It is strongly advised to invoke Zplugs init.zsh script
    if [[ ! -d $ZPLUGSETTINGS ]]; then
        git clone https://github.com/zplug/zplug $ZPLUGSETTINGS/
        source $ZPLUGSETTINGS/init.zsh && zplug update --self
    else
        source $ZPLUGSETTINGS/init.zsh
    fi

    ####################
    # rq_installling
    ####################
    # Install plugins if there are plugins that have not been installed
    if ! zplug check --verbose; then
        printf "Install? [y/N]: "
        if read -q; then
            echo; zplug install
        fi
    fi
else
    echo "INFO: settings for Zsh packagemanager"
    echo "INFO: Package manager Zplug expected"
    echo "Q5q60nq_oq_packagemanager_zsh value: $Q5q60nq_oq_packagemanager_zsh"
fi
